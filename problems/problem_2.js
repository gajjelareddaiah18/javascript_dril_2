// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"


function Problem_2(inventory){
    if(!inventory ||typeof inventory ==="Object"){
        return console.log("inventory not avalible");
    }
    let last_car=inventory.slice(inventory.length-1)
    return  console.log(`Last car is a ${last_car[0].car_make} ${last_car[0].car_model}`)
    
}



module.exports={
    Problem_2
}
