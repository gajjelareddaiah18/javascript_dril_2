


// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem_1(inventory,id){
    if(!inventory ||typeof inventory ==="Object"){
        return console.log("inventory not avalible");
    }

    let car_id=inventory.filter(Element=>Element.id==id);
    return  console.log(`Car ${id} is a ${car_id[0].car_year} ${car_id[0].car_make} ${car_id[0].car_model}`)

        
}

module.exports={
    problem_1

}

