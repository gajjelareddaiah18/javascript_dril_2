
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
function problem_5(inventory,year){
    if(!inventory ||typeof inventory ==="Object"){
        return console.log("inventory not avalible");
    }
    let car_list=[]
   inventory.map((Element)=>{
    if(Element.car_year<year){
        car_list.push(Element.car_model)
    }
   })
   

   return (console.log(car_list, `total_cars : ${car_list.length}`))

   
}

module.exports={
    problem_5
}
