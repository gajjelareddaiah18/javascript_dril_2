// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function Problem_6(inventory,carnames){
    if(!inventory ||typeof inventory ==="Object"){
        return console.log("inventory not avalible");
    }
    let BMW_Audi=[]
    inventory.map((Element)=>{
        if(Element.car_make== carnames){
            BMW_Audi.push(Element)
        }
    })
    return BMW_Audi
    
}

module.exports={
    Problem_6

}