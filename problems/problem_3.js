// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
  

function Problem_3(inventory){
    if(!inventory ||typeof inventory ==="Object"){
        return console.log("inventory not avalible");
    }
    let sorting=inventory.map(Element=>Element.car_model.toLowerCase())
    return sorting.sort()
    
    }




module.exports={
    Problem_3

}